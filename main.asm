global _start

%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 256
%define NEW_STRING_SYMBOL 10

extern find_word

section .rodata
overflow_mess: db NEW_STRING_SYMBOL, "Превышен размер буффера <=255", NEW_STRING_SYMBOL, 0
not_found_mess: db "Нет значения по такому ключу", NEW_STRING_SYMBOL, 0

section .bss
buffer: resb BUFFER_SIZE


section .text

_start:
    mov rdi, buffer; начало буфера
    mov rsi, BUFFER_SIZE
    call read_word; после в rax - адрес слова, rdx - длина слова
    .check_overflow:
    test rax, rax
    je .out_bounds_size

    .check_word:
    mov rdi, rax; адрес ключа
    mov rsi, next_element; в rsi кладем указатель на следующуее вхождение
    push rdx; сохраняем регистр
    call find_word; в rax результат
    pop rdx; возвращаем регистр
    .isHave_key:
    test rax, rax; проверка на значение по ключуSegmentation fault (core dumped)
    je .not_found_key

    .continue:
    mov rdi, rax; кладем адрес нужного вхождения в rdi
    add rdi, 9; пропускаем указатель на следующее вхождение 
    add rdi, rdx; пропускаем ключ
    call print_string; печатаем
    jmp .end

    .out_bounds_size:
        mov rdi, overflow_mess     ; передаём строку
        call error          	   ; печатаем ошибку
        jmp .end

    .not_found_key:
        mov rdi, not_found_mess   ; передаём строку
        call error                ; печатаем ошибку
        jmp .end

    .end:
      call print_newline          ; переводим строку
      xor rdi, rdi
      call exit                   ; завершаем программу


