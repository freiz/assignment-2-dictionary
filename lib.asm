section .text

%define EXIT 60
%define STDOUT 1
%define NEW_STRING 10
%define NULL_TERMINATE 0
%define MIN 48
%define MAX 57
%define MINUS_CHAR 45
%define SPACE 20
%define TAB 9
%define STDERR 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_message
global error
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall


; char *str = "this is line";
; string_length(str);
; div rbx; rax - частное; rdx - остаток
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax 

.loop:
    mov sil, byte[rdi+rax]
    test sil, sil
    jz .end
    inc rax
    jmp .loop

.end:
    ret

; rdi принимает указатель, rsi принимает сообщение об ошибке
print_message:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, rcx
    mov rax, 1
    syscall
    ret

error:
    mov rcx, STDERR          
    call print_message


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rсx, rdi
    call string_length
    mov rdi, STDOUT
    mov rsi, rсx
    mov rdx, rax
    mov rax, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, rax
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA) 
print_newline:
    mov rdi, NEW_STRING
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov rsi, 1
    push rcx
    dec rsp
    mov rcx, NEW_STRING

.loop:
    xor rdx, rdx
    div rcx
    add rdx, MIN
    dec rsp
    mov [rsp], dl
    inc rsi
    test rax, rax
    jnz .loop

.print_d:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    pop rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .positive
    push rdi
    mov rdi, MINUS_CHAR; 
    call print_char
    pop rdi
    neg rdi
    
.positive:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax

.loop:
    mov bl, [rdi+rax]
    cmp bl, byte[rsi+rax]
    jne .noequals
    test bl, bl
    je .equals
    inc rax
    jmp .loop

.noequals:
    xor rax, rax; return false
    ret

.equals:
    mov rax, 1; return true
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rsi, rsp 
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1; size_char;
    syscall
    test rax, rax
    jz .stop
    mov al, [rsp]

.stop:
    inc rsp
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx   
    dec rsi   

.loop:
    cmp rdx, rsi
    jge .stop
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    test rax, rax
    jz .terminate
    push rax
    push rdi

    mov dil, al
    call .is_space
    test rax, rax
    pop rdi
    pop rax
    jz .if_not_space
    test rdx, rdx
    jz .loop
    jmp .terminate

.is_space:
    cmp dil, NEW_STRING
    je .true
    cmp dil, SPACE
    je .true
    cmp dil, TAB
    je .true

.false:
    xor rax, rax; return 0, because result false
    ret

.true:
    mov rax, 0xF
    ret

.if_not_space:
    mov [rdi+rdx], rax
    inc rdx
    jmp .loop

.terminate:
    mov byte[rdi+rdx], NULL_TERMINATE;
    mov rax, rdi
    ret
.stop:
    xor rax, rax
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, NEW_STRING;

.loop:
    push rsi
    mov sil, [rdi]
    test sil, sil
    je .stop
    cmp sil, NEW_STRING
    je .stop
    cmp sil, MIN
    jb .stop
    cmp sil, MAX
    ja .stop
    sub sil, MIN
    mul r8
    add rax, rsi
    inc rdi
    pop rsi
    inc rsi
    jmp .loop

.stop:
    pop rdx
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi;
    call parse_uint
    pop rdi
    test rdx, rdx
    jne .stop
    mov sil, [rdi]
    inc rdi
    push rsi
    call parse_uint
    inc rdx
    pop rsi
    cmp sil, MINUS_CHAR
    jne .stop
    neg rax
    jne .stop

.stop:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .end
.loop:
    mov dl, [rdi+r8]
    mov [rsi+r8], dl
    inc r8
    cmp r8, rax
    jle .loop
.end:
    xor rax, rax
    ret
