extern string_equals
section .text
%define POINTER_SIZE 8
global find_word

find_word:
    .nextElem:
        add rsi, POINTER_SIZE;
        push rdi 
        push rsi
        call string_equals
        pop rsi 
        pop rdi
        cmp rax, 1
        je .found
        sub rsi, POINTER_SIZE
        mov r10, [rsi]
        mov rsi, r10

        test r10, r10
        jnz .nextElem

        .not_found:
        xor rax, rax
        ret

    .found:
        sub rsi, POINTER_SIZE
        mov rax, rsi
        ret
    
        
